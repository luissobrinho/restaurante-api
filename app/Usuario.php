<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Usuario extends Model
{
    protected $fillable = [
        'nome',
        'data_nascimento',
        'cargo',
        'nivel',
        'id_user',
    ];

    protected  $dates = [
        'created_at',
        'updated_at',
        'data_nascimento'
    ];

    public function user() {
        return $this->belongsTo('\App\User', 'id_user');
    }
}
