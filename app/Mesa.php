<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mesa extends Model
{
    protected $fillable = [
        'numero',
        'quantidade',
        'disponivel',
        'local',
    ];

    protected  $dates = [
        'created_at',
        'updated_at',
    ];

    protected $casts = [
        'numero' => 'int',
        'quantidade' => 'int',
        'disponivel' => 'boolean',
    ];
}
