<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Taxa extends Model
{
    protected $fillable = [
        'valor',
        'tipo',
        'descricao',
        'disponivel',
    ];

    protected  $dates = [
        'created_at',
        'updated_at',
    ];

    protected $casts = [
      'valor' => 'float',
      'tipo' => 'int',
      'disponivel' => 'boolean',
    ];
}
