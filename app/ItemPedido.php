<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemPedido extends Model
{
    protected $fillable = [
        'id_pedido',
        'id_produto',
        'observacao',
        'quantidade',
        'tipo',
        'novo',
    ];

    protected  $dates = [
        'created_at',
        'updated_at',
    ];

    protected $casts = [
        'id_pedido' => 'int',
        'id_produto' => 'int',
        'quantidade' => 'int',
        'novo' => 'boolean',
    ];

    public function pedido() {
        return $this->belongsTo('\App\Pedido', 'id_pedido');
    }

    public function produto() {
        return $this->belongsTo('\App\Produto', 'id_produto');
    }

    public function taxa() {
        return $this->belongsTo('\App\Taxa', 'id_produto');
    }

}
