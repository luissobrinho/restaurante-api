<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Carrinho extends Model
{
    protected $fillable = [
        'id_produto',
        'id_user',
        'id_mesa',
        'quantidade',
        'observacao',
        'complemento',
        'valor_adicional'
    ];

    protected  $dates = [
        'created_at',
        'updated_at',
    ];

    protected $casts = [
        'id_produto' => 'int',
        'id_user' => 'int',
        'id_mesa' => 'int',
        'quantidade' => 'int',
        'complemento' => 'array',
        'valor_adicional' => 'float',
    ];

    public function produto() {
        return $this->belongsTo('\App\Produto', 'id_produto');
    }

    public function user() {
        return $this->belongsTo('\App\User', 'id_user');
    }

    public function mesa() {
        return $this->belongsTo('\App\Mesa', 'id_mesa');
    }
}
