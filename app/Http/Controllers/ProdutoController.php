<?php

namespace App\Http\Controllers;

use App\Produto;
use Illuminate\Http\Request;

class ProdutoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $produtos = Produto::with('categoria')
            ->paginate(20);

        if(!$produtos->total()) {
            return response()->json([
                'message' => 'Não há produtos cadastrado'
            ], 404);
        }

        return response()->json($produtos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $produto = new Produto();

        $produto->fill($request->all());
        $produto->save();

        return response()->json($produto, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $produto = Produto::find($id);

        if(!$produto) {
            return response()->json([
                'message' => 'Produto não encontrado'
            ], 404);
        }

        return response()->json($produto);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $produto = Produto::find($id);

        if(!$produto) {
            return response()->json([
                'message' => 'Produto não encontrado'
            ], 404);
        }

        $produto->fill($request->all());
        $produto->update();

        return response()->json($produto);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $produto = Produto::find($id);

        if(!$produto) {
            return response()->json([
                'message' => 'Produto não encontrado'
            ], 404);
        }

        $produto->delete();

        return response()->json($produto);
    }

    public function available($id) {
        $produto = Produto::find($id);

        if(!$produto) {
            return response()->json([
                'message' => 'Mesa não encontrada'
            ], 404);
        }

        $produto->disponivel = !$produto->disponivel;
        $produto->update();

        return response()->json($produto);
    }

    public function menu($id_categoria) {
        $produtos = Produto::where('disponivel', '=', true)
            ->where('tipo', '=', 1)
            ->where('id_categoria', '=', $id_categoria)
            ->paginate(10);

        if(!$produtos->total()) {
            return response()->json([
                'message' => 'Não há produtos disponíveis'
            ], 404);
        }

        return response()->json($produtos);
    }
}
