<?php

namespace App\Http\Controllers;

use App\Carrinho;
use App\Mesa;
use App\Produto;
use App\Usuario;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;

class CarrinhoController extends Controller
{

    private $complementoString = ' Complementos: ';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $mesas = Mesa::join('carrinhos', 'mesas.id', 'carrinhos.id_mesa')
            ->addSelect('mesas.*')
            ->get();

        if(!$mesas) {
            return response()->json([
                'message' => 'Não mesas com produtos no carrinho'
            ], 404);
        }

        return response()->json($mesas);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        $isCarrinho = Carrinho::where('id_produto', '=', $request->id_produto)
//            ->where('id_mesa', '=', $request->id_mesa)
//            ->get();

//        if(count($isCarrinho)) {
//            return $this->update($request, $isCarrinho[0]->id);
//        }

        $carrinho = new Carrinho();
        $carrinho->fill($request->all());

        $valor_adicional = 0.0;

        if(count($request->complemento)) {

            foreach($request->complemento as $id_complemento) {
                $complemento = Produto::find($id_complemento);
                $valor_adicional += $complemento->valor;
                $this->complementoString .= $complemento->nome . '; ';
            }

            $carrinho->observacao .= $this->complementoString;
        }

        $carrinho->valor_adicional = $valor_adicional;

        $carrinho->save();

        return response()->json($carrinho);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $carrinho = Carrinho::with('produto')->where('id_mesa', '=', $id)->get();

        if(!$carrinho->count()) {
            return response()->json([
                'message' => 'Nem um produto no carrinho'
            ], 404);
        }

        return response()->json($carrinho);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $carrinho = Carrinho::find($id);

        if(!$carrinho) {
            return response()->json([
                'message' => 'Produto não encontrado no carrinho'
            ], 404);
        }

        $carrinho->fill($request->all());

        $valor_adicional = 0.0;

        if(count($request->complemento)) {

            foreach($request->complemento as $id_complemento) {
                $complemento = Produto::find($id_complemento);
                $valor_adicional += $complemento->valor;
                $this->complementoString .= $complemento->nome . '; ';
            }

            $carrinho->observacao .= $this->complementoString;
        }

        $carrinho->valor_adicional = $valor_adicional;

        $carrinho->update();

        return response()->json($carrinho);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $carrinho = Carrinho::find($id);

        if(!$carrinho) {
            return response()->json([
                'message' => 'Produto não encontrado no carrinho'
            ], 404);
        }

        $carrinho->delete();

        return response()->json($carrinho);
    }

    public function showToArray($id_mesa) {
        $carrinho = Carrinho::with('produto')
            ->where('id_mesa', '=', $id_mesa)
            ->get();

        if(!$carrinho->count()) {
            return [];
        }

        return $carrinho->toArray();
    }
}
