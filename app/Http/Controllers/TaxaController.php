<?php

namespace App\Http\Controllers;

use App\Taxa;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;

class TaxaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $taxas = Taxa::all();

        if(!$taxas->count()) {
            return response()->json([
                'message' => 'Não há taxas cadastrada'
            ], 404);
        }

        return response()->json($taxas);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $taxa = new Taxa();

        $taxa->fill($request->all());
        $taxa->save();

        return response()->json($taxa, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $taxa = Taxa::find($id);

        if(!$taxa) {
            return response()->json([
                'message' => 'Taxa não encontrada'
            ], 404);
        }

        return response()->json($taxa);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $taxa = Taxa::find($id);

        if(!$taxa) {
            return response()->json([
                'message' => 'Taxa não encontrada'
            ], 404);
        }

        $taxa->fill($request->all());
        $taxa->update();

        return response()->json($taxa);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $taxa = Taxa::find($id);

        if(!$taxa) {
            return response()->json([
                'message' => 'Taxa não encontrada'
            ], 404);
        }

        $taxa->delete();

        return response()->json($taxa);
    }

    public function available($id) {
        $taxa = Taxa::find($id);

        if(!$taxa) {
            return response()->json([
                'message' => 'Mesa não encontrada'
            ], 404);
        }

        $taxa->disponivel = !$taxa->disponivel;
        $taxa->update();

        return response()->json($taxa);
    }

    public function showOnArray() {
        $taxas = Taxa::all()->where('disponivel', '=', true);

        if(!$taxas->count()) {
            return [];
        }

        return $taxas->toArray();
    }
}
