<?php

namespace App\Http\Controllers;

use App\Mesa;
use Illuminate\Http\Request;

class MesaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $mesas = Mesa::where('id', '<>', 0)
            ->orderBy('numero')
            ->get();

        if(!$mesas) {
            return response()->json([
                'message' => 'Não há Mesas cadastradas'
            ], 404);
        }

        return response()->json($mesas);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $mesa = new Mesa();

        $mesa->fill($request->all());
        $mesa->save();

        return response()->json($mesa, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $mesa = Mesa::find($id);

        if(!$mesa) {
            return response()->json([
                'message' => 'Mesa não encontrada'
            ], 404);
        }

        return response()->json($mesa);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $mesa = Mesa::find($id);

        if(!$mesa) {
            return response()->json([
                'message' => 'Mesa nã encontrada'
            ], 404);
        }

        $mesa->fill($request->all());
        $mesa->update();

        return response()->json($mesa);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $mesa = Mesa::find($id);

        if(!$mesa) {
            return response()->json([
                'message' => 'Mesa não encontado'
            ], 404);
        }

        $mesa->delete();

        return response()->json($mesa);
    }

    public function available($id) {
        $mesa = Mesa::find($id);

        if(!$mesa) {
            return response()->json([
                'message' => 'Mesa não encontrada'
            ], 404);
        }

        $mesa->disponivel = !$mesa->disponivel;
        $mesa->update();

        return response()->json($mesa);
    }
}
