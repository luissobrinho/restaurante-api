<?php

namespace App\Http\Controllers;

use App\Complemento;
use App\Produto;
use Illuminate\Http\Request;

class ComplementoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $complementos = Produto::where('tipo', '=', 2)->get();

        if(!$complementos->count()) {
            return response()->json([
                'message' => 'Não há complemento cadastrado'
            ], 404);
        }

        return response()->json($complementos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $isComplemento = Complemento::all()
            ->where('id_complemento', '=', $request->id_complemento)
            ->where('id_produto', '=', $request->id_produto)
            ->count();

        if($isComplemento) {
            return response()->json([
                'message' => 'Complemento já cadastrado'
            ], 401);
        }

        $complemento = new Complemento();

        $complemento->fill($request->all());
        $complemento->save();

        return response()->json($complemento, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $complementos = Complemento::with('complemento')
            ->where('id_produto', '=', $id)
            ->get();

        if(!$complementos->count()) {
            return response()->json([
                'message' => 'Complemento(s) não encontrado(s)'
            ], 404);
        }

        return response()->json($complementos);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $complemento = Complemento::find($id);

        if(!$complemento) {
            return response()->json([
                'message' => 'Complemento não encontrado'
            ], 404);
        }

        $complemento->delete();

        return response()->json($complemento);
    }
}
