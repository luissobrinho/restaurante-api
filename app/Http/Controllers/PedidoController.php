<?php

namespace App\Http\Controllers;

use App\ItemPedido;
use App\Pedido;
use Illuminate\Http\Request;

class PedidoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pedidos = Pedido::where('id', '<>', 0)->paginate(20);

        if(!$pedidos->values()->count()) {
            return response()->json([
                'message' => 'Não há pedidos cadastrado'
            ], 404);
        }

        return response()->json($pedidos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $pedidoCurrent = Pedido::where('id_mesa', $request->id_mesa)
            ->where('status', 2)
            ->orWhere('status', 1)
            ->get();

        if(count($pedidoCurrent) > 0) {
            return $this->update($request, $pedidoCurrent[0]->id);
        }

        $pedido = new Pedido();

        $pedido->fill($request->all());
        $pedido->save();
        ////////PEGAR PRODUTOS////////
        $carrinhoCtrl = new CarrinhoController();
        $carrinho = $carrinhoCtrl->showToArray($request->id_mesa);

        if(!count($carrinho)) {
            return response()->json([
                'message' => 'Não há itens no carrinho'
            ], 401);
        }

        foreach($carrinho as  $item) {
            /////////ITEM PEDIDO//////////
            $item = (object) $item;
            $produto = (object) $item->produto;

            $pedido->valor += (($produto->valor + $item->valor_adicional) * $item->quantidade);
            $itemPedido = new ItemPedido();

            $itemPedido->fill([
                'id_pedido' => $pedido->id,
                'id_produto' => $item->id_produto,
                'observacao' => $item->observacao,
                'quantidade' => $item->quantidade,
            ]);
            $itemPedido->save();

            $currentCarrinhoCtrl = new CarrinhoController();

            $currentCarrinhoCtrl->destroy($item->id);
        }

        $taxaCtrl = new TaxaController();

        $taxas = $taxaCtrl->showOnArray();

        $valorTaxa = 0;

        foreach($taxas as $taxa) {
            $taxa = (object) $taxa;
            if($taxa->disponivel) {
                $itemPedido = new ItemPedido();

                $itemPedido->fill([
                    'id_pedido' => $pedido->id,
                    'id_produto' => $taxa->id,
                    'observacao' => $taxa->descricao,
                    'quantidade' => 1,
                    'tipo' => 'TAXA'
                ]);

                $itemPedido->save();
                if($taxa->tipo == 0) {
                    $valorTaxa += $taxa->valor;
                } else {
                    $valorTaxa += ($pedido->valor * ($taxa->valor / 100));
                }
            }

        }

        $pedido->valor += $valorTaxa;

        $pedido->update();

        return response()->json($pedido, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pedido = Pedido::with('mesa')
            ->with('user')
            ->where('id', $id)
            ->get();

        if(!$pedido) {
            return response()->json([
                'message' => 'Pedido não encontrado'
            ], 404);
        }

        return response()->json($pedido);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $pedido = Pedido::find($id);

        if(!$pedido) {
            return response()->json([
                'message' => 'Pedido não encontrado'
            ], 404);
        }
        ////////PEGAR PRODUTOS////////
        $carrinhoCtrl = new CarrinhoController();
        $carrinho = $carrinhoCtrl->showToArray($pedido->id_mesa);

        if(!count($carrinho)) {
            return response()->json([
                'message' => 'Não há itens no carrinho'
            ], 401);
        }

        foreach($carrinho as  $item) {
            /////////ITEM PEDIDO//////////
            $item = (object) $item;
            $produto = (object) $item->produto;

            $pedido->valor += (($produto->valor + $item->valor_adicional) * $item->quantidade);

            $itemPedido = new ItemPedido();

            $itemPedido->fill([
                'id_pedido' => $pedido->id,
                'id_produto' => $item->id_produto,
                'observacao' => $item->observacao,
                'quantidade' => $item->quantidade,
                'novo' => true
            ]);
            $itemPedido->save();

            $currentCarrinhoCtrl = new CarrinhoController();

            $currentCarrinhoCtrl->destroy($item->id);
        }

        $pedido->status = 1;
        $pedido->update();

        return response()->json($pedido);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }

    public function statusOnView($status) {

        if($status !== "0") {
            $pedidos = Pedido::where('status', '=', $status)
                //->whereDate('created_at', date('Y-m-d'))
                ->get();
        } else {
            $pedidos = Pedido::where('status', '<>', $status)
                ->whereDay('created_at', date('d'))
                ->whereMonth('created_at', date('m'))
                ->get();
        }

        if(!count($pedidos)) {
            return response()->json([
                'message' => 'Não há pedidos cadastrado'
            ], 404);
        }

        return response()->json($pedidos);
    }

    public function movePedido($id_pedido, $status) {

        $pedido = Pedido::find($id_pedido);

        if(!$pedido) {
            return response()->json([
                'message' => 'Pedido não encontrado'
            ], 404);
        }

        $pedido->status = $status;

        if($status == 2) {
            $itens = ItemPedido::all()
                ->where('id_pedido', '=', $id_pedido);

            $itens->map(function($item) {
                $item->novo = false;
                $item->update();
            });
        }

        $pedido->update();

        return response()->json($pedido);
    }

    public function pagar(Request $request, $id) {
        $pedido = Pedido::find($id);

        if(!$pedido) {
            return response()->json([
                'message' => 'Pedido não encontrado'
            ], 404);
        }

        $pedido->fill($request->all());
        $pedido->update();
    }
}
