<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Complemento extends Model
{
    protected $fillable = [
        'id_produto',
        'id_complemento',
    ];

    protected  $dates = [
        'created_at',
        'updated_at',
    ];

    public function produto() {
        return $this->belongsTo('\App\Produto', 'id_produto');
    }

    public function complemento() {
        return $this->belongsTo('\App\Produto', 'id_complemento');
    }
}
