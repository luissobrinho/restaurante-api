<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Favorito extends Model
{
    protected $fillable = [
        'id_produto',
    ];

    protected  $dates = [
        'created_at',
        'updated_at',
    ];

    protected $hidden = [
      'id_produto'
    ];

    public function produto() {
        return $this->belongsTo('\App\Produto', 'id_produto');
    }
}
