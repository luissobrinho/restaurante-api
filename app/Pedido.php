<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pedido extends Model
{
    protected $fillable = [
        'id_user',
        'id_mesa',
        'valor',
        'forma_pagamento',
        'pago',
        'status',
    ];

    protected  $dates = [
        'created_at',
        'updated_at',
    ];

    protected $casts = [
        'id_user' => 'int',
        'id_mesa' => 'int',
        'valor' => 'float',
        'forma_pagamento' => 'int',
        'pago' => 'float',
        'status' => 'int',
    ];

    public function mesa() {
        return $this->belongsTo('\App\Mesa', 'id_mesa');
    }

    public function user() {
        return $this->belongsTo('\App\Usuario', 'id_user');
    }
}
