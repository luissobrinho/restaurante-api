<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Produto extends Model
{
    protected $fillable = [
        'nome',
        'descricao',
        'tipo',
        'disponivel',
        'valor',
        'foto',
        'thumb',
        'id_categoria',
    ];

    protected  $dates = [
        'created_at',
        'updated_at',
    ];

    protected $casts = [
        'tipo' => 'int',
        'disponivel' => 'boolean',
        'valor' => 'float'
    ];

    public function categoria() {
        return $this->belongsTo('\App\Categoria', 'id_categoria')
            ->addSelect(['id', 'nome']);
    }
}
