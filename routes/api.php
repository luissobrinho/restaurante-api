<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/authenticate/{app_token}', 'UserController@authenticate')
    ->where(['app_token' => 'hMuOMqr9OM4obFZdKircuxjQvtzURWcR'])
    ->name('user.authenticate');
Route::get('/authenticate/refresh', 'UserController@refresh')
    ->name('user.refresh');
Route::middleware('jwt.auth')->group(function() {
    ///////USER
    Route::resource('user', 'UserController');
    Route::get('/user/get-info', 'UserController@getInfo');
    //////USUÁRIO
    Route::resource('usuario','UsuarioController');
    /////CATEGORIA
    Route::resource('categoria', 'CategoriaController');
    /////MESA
    Route::resource('mesa', 'MesaController');
    Route::get('mesa/{id}/available', 'MesaController@available')
        ->name('mesa.available');
    /////PRODUTO
    Route::resource('produto', 'ProdutoController');
    Route::get('produto/{id}/available', 'ProdutoController@available')
        ->name('produto.available');
    Route::get('produto/{id_categoria}/menu', 'ProdutoController@menu')
        ->name('produto.menu');
    /////FAVORITO
    Route::resource('favorito', 'FavoritoController');
    /////COMPLEMENTO
    Route::resource('complemento', 'ComplementoController');
    /////CARRINHO
    Route::resource('carrinho', 'CarrinhoController');
    /////TAXA
    Route::resource('taxa', 'TaxaController');
    Route::get('taxa/{id}/available', 'TaxaController@available')
        ->name('taxa.available');
    /////PEDIDO
    Route::resource('pedido', 'PedidoController');
    Route::get('pedido/status/{status}', 'PedidoController@statusOnView')
        ->name('pedido.statusOnView');
    Route::get('pedido/{pedido}/move/{status}', 'PedidoController@movePedido')
        ->name('pedido.movePedido');
    Route::post('pedido/{pedido}/pagar', 'PedidoController@pagar')
        ->name('pedido.pagar');
    /////ITEM PEDIDO
    Route::resource('item-pedido', 'ItemPedidoController');

});
