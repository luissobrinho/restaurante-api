<?php

use Illuminate\Database\Seeder;

class UserSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'CPF' => '108.268.714-61',
            'numero' => '82982376249',
            'usuario' => 'user',
            'matricula' => 'as132044v0',
            'token' => '5485',
            'email' => 'user@mail.com',
            'ativo' => true,
            'password' => bcrypt('123456'),
        ]);
    }
}
