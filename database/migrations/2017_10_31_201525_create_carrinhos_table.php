<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarrinhosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('carrinhos', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('id_produto');
            $table->foreign('id_produto')
                ->references('id')
                ->on('produtos')
                ->onDelete('cascade');
            $table->unsignedInteger('id_user');
            $table->foreign('id_user')
                ->references('id')
                ->on('usuarios')
                ->onDelete('cascade');
            $table->unsignedInteger('id_mesa');
            $table->foreign('id_mesa')
                ->references('id')
                ->on('mesas')
                ->onDelete('cascade');
            $table->integer('quantidade');
            $table->text('observacao')->nullable();
            $table->text('complemento')->nullable();
            $table->float('valor_adicional', 9,2)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('carrinhos');
    }
}
