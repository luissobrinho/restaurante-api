<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePedidosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pedidos', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('id_user');
            $table->foreign('id_user')
                ->references('id')
                ->on('usuarios')
                ->onDelete('cascade');
            $table->unsignedInteger('id_mesa');
            $table->foreign('id_mesa')
                ->references('id')
                ->on('mesas')
                ->onDelete('cascade');
            $table->float('valor', 9, 2);
            $table->integer('forma_pagamento');
            $table->float('pago', 9, 2);
            $table->integer('status')
                ->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pedidos');
    }
}
