<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemPedidosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_pedidos', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('id_pedido');
            $table->foreign('id_pedido')
                ->references('id')
                ->on('pedidos')
                ->onDelete('cascade');
            $table->unsignedInteger('id_produto');
            $table->foreign('id_produto')
                ->references('id')
                ->on('produtos')
                ->onDelete('cascade');
            $table->text('observacao');
            $table->integer('quantidade');
            $table->enum('tipo', ['PRODUTO', 'TAXA'])
                ->default('PRODUTO');
            $table->boolean('novo')
                ->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('item_pedidos');
    }
}
