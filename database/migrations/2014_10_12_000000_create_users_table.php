<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('CPF', 100)->unique()->nullable();
            $table->string('numero', 100)->unique()->nullable();
            $table->string('usuario', 100)->unique()->nullable();
            $table->string('matricula', 100)->unique()->nullable();
            $table->string('token', 100)->unique()->nullable();
            $table->string('email', 100)->unique()->nullable();
            $table->boolean('ativo')->default(true);
            $table->string('password')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
